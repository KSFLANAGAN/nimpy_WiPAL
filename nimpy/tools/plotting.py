import numpy as np
import matplotlib.pyplot as plt

'''
this module contains nimpy plotting routines and helpers
It depends on:
    numpy (as np)
    matplotlib.pyplot (as plt)
'''
def get_lims(data, num=50, frac=0.01):
    ''' handy function to guess levels and cmap for data

    get_lims chooses levels based on the min and max
    of data input and then either a divergent colormap
    (if +/- values exist) or a simple rainbow colormap 
    for one sign only values

    Args:
        data (np.ndarray): the data to use
        num (int, default=50): the number of levels to return
        frac (float, default=0.01): the fraction of (max-min)
            to use to consider the data as having a negative
            or positive value

    Returns:
        (cmap, levels): cmap (str): the colormap name
                        levels (np.ndarray): the contour levels
    '''
    mx = np.nanmax(data)
    mn = np.nanmin(data)
    rg = mx - mn
    ### if the minimum is less than some tiny negative number
    ### and the maximum is greater than some tiny number
    if mn < -frac*rg and mx > frac*rg:
        cmap = 'coolwarm'
        ### want the levels to be symmetric about 0
        edge = max([np.abs(mn), mx]) 
        ### ensures 0 is a level if odd number of levels
        if num %2 == 0:
            num += num
        levels = np.linspace(-edge, edge, num, endpoint=True)
    ### if the maximum magnitude is positive, use rainbow
    elif np.abs(mx) > np.abs(mn):
        cmap = 'rainbow'
        levels = np.linspace(mn, mx, num, endpoint=True)
    ### if the maximum magnitude is positive use reversed
    ### rainbow colormap
    else:
        cmap = 'rainbow_r'
        levels = np.linspace(mn, mx, num, endpoint=True)
    return cmap, levels

def contourf(data, grid, fax=None, cmap_levels=None, title=None):
    ''' simple filled contourf plot

    contourf is a simple wrapper for matplotlib.pyplot.contourf
    it includes a colorbar, variable cmap and variable contour
    levels.

    Args:
        data (np.ndarray ndim=2): data you wish to plot
        grid (tuple of np.ndarray's): the X and Y arrays for the data
        fax (tuple, default=None): the figure and axes to use
            if not provided, this function will create it's own
        cmap_levels (tuple (str, np.ndarray)): the cmap and
            contour levels to use. If not provided get_lims is
            called to get a good cmap and levels
        title (str, default is None): If you want to add a title

    Returns:
        f (matplotlib.pyplot.figure): the figure
        ax (matplotlib.pyplot.axes): the axes

    Note: does not actually create plot window. to do so, call
        matplotlib.pyplot.show()
    '''
    if cmap_levels is None:
        cmap, levels = get_lims(data)
    else:
        cmap, levels = cmap_levels
    if fax is None:
        f, ax = plt.subplots()
    else:
        f, ax = fax
    try: 
        cf = ax.contourf(grid[0], grid[1], data, levels=levels, cmap=cmap, extend='both')
    except ValueError:
        cf = ax.contourf(grid[0], grid[1], data)
    f.colorbar(cf, ax=ax, fraction=0.05)
    ax.set_aspect('equal')
    if title is not None:
        ax.set_title(title)
    return f,ax

def vec_contourf(tor, pol, grid, psi=None, title=None, tor_cmap_levels=None, pol_cmap_levels=None, psi_levels=None):
    ''' simple filled contourf plot for vector fields

    vec_contourf plots the toroidal component and poloidal magnitude
    for a vector field. If psi is included, it also plots streamlines
    over the poloidal magnitude plot.

    Args:
        tor (np.ndarray): toroidal component (f2)
        pol (np.ndarray): poloidal magnitude (np.sqrt(f1**2+f3**2))
        grid (tuple of np.ndarray's): the X and Y arrays for the data
        psi (np.ndarray, optional): the streamfunction
        title (str, optional): a title for the plot
        tor_cmap_levels (tuple, optional): the toroidal
            colormap (str) and contour levels (np.ndarray)
        pol_camp_levels (tuple, optional): the poloidal
            colormap (str) and contour levels (np.ndarray)
        psi_levels (np.ndarray): the contour levels for the 
            streamfunction
        if any of these cmaps and levels aren't provided, get_lims()
        will be called to find best cmap and levels

    Note: does not actually create plot window. to do so, call
        matplotlib.pyplot.show()
    '''
    if psi is not None and psi_levels is None:
        _, psi_levels = get_lims(psi, num=12)

    f, axs = plt.subplots(1, 2, sharex=True, sharey=True)
    contourf(tor, grid, (f, axs[0]), tor_cmap_levels, title='toroidal component') 
    contourf(pol, grid, (f, axs[1]), pol_cmap_levels, title='poloidal magnitude')

    if psi is not None:
        if psi_levels is None:
            _, psi_levels = get_lims(psi, num=12)
        axs[1].contour(grid[0], grid[1], psi, levels=psi_levels, colors='k', linewidths=1.5)
    
    if title is not None:
        f.suptitle(title)
